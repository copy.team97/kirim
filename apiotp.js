const express = require('express');
const { Client } = require('whatsapp-web.js');
const qrcode = require('qrcode-terminal');

// Buat instance npm client WhatsApp
const app = express();
const client = new Client();

app.use(express.json());

// Event saat QR code untuk login diterima
client.on('qr', qr => {
    // Tampilkan QR code di terminal
    qrcode.generate(qr, { small: true });

    // Kirim respon HTML yang berisi gambar QR code
    const qrCodeDataUrl = 'data:image/png;base64,' + qr;
    const html = `<img src="${qrCodeDataUrl}" alt="QR Code">`;
    // Kirimkan respon ke browser
    // Pastikan parameter `res` didefinisikan di dalam fungsi event 'qr'
    res.send(html);
});

// Endpoint untuk mengirim pesan WhatsApp
app.post('/send-message', (req, res) => {
    const { phoneNumber, message } = req.body;
  
    // Kirim pesan ke nomor HP yang ditentukan
    client.sendMessage(phoneNumber+'@c.us', message)
      .then(() => {
        res.status(200).json({ success: true, message: 'Pesan berhasil dikirim' });
      })
      .catch((error) => {
        res.status(500).json({ success: false, message: 'Gagal mengirim pesan', error: error.message });
      });
});

// Event saat client terhubung
client.on('authenticated', (session) => {
    console.log('Authenticated:', session);
});

// Event saat client terputus
client.on('auth_failure', (message) => {
    console.error('Authentication failed:', message);
});

// Event saat client berhasil terhubung
client.on('ready', () => {
    console.log('Client is ready!');
});

// Mulai sesi client WhatsApp
client.initialize();

// Jalankan server
app.listen(3000, () => {
  console.log('Server berjalan di http://localhost:3001');
});
